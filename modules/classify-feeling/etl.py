import pandas as pd
from toyai.Trainer import Trainer
from transformers import BertTokenizer
import torch
from sklearn.model_selection import train_test_split
from torch.utils.data import TensorDataset
from sklearn.preprocessing import LabelEncoder
import matplotlib.pyplot as plt
from toyai import ETL as BaseETL
from torch.utils.data import DataLoader


def tokenize(texts, max_length=128):
    tokenizer = BertTokenizer.from_pretrained("bert-base-multilingual-cased")
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    encodings = tokenizer(
        texts,
        padding=True,
        truncation=True,
        max_length=max_length,
        return_tensors="pt",
    ).to(device)
    return encodings


class ETL(BaseETL):
    def extract(self, input_dir) -> dict[str, any]:
        data = pd.read_csv(
            "datasets/classify_feeling_v1c_202406051629.csv", delimiter="|"
        )
        labels = pd.read_csv(
            "datasets/classify_feeling_v1c_labels_202406051629.csv", delimiter=","
        )
        data.to_csv(self.get_save_filepath(f"input.csv"), index=False)
        labels.to_csv(self.get_save_filepath(f"labels.csv"), index=False)
        return {
            "data": data,
            "labels": labels,
        }

    def transform(self, data: dict[str, any]) -> dict[str, any]:
        # Combine language and message columns
        combined = (
            "lang:" + data["data"]["lang"] + ",message:" + data["data"]["message"]
        )

        combined.to_csv(self.get_save_filepath(f"combined.csv"), index=False)
        messages = combined.values
        feelings = data["data"]["feeling"].values

        # Encode the labels using LabelEncoder
        label_encoder = LabelEncoder()
        encoded_labels = label_encoder.fit_transform(feelings)

        # Split the data into training and test sets
        train_texts, test_texts, train_labels, test_labels = train_test_split(
            messages,
            encoded_labels,
            test_size=float(self.args["test_size"]),
            random_state=42,
        )

        # Tokenize the texts
        train_encodings = tokenize(
            list(train_texts), max_length=int(self.args["max_length"])
        )
        val_encodings = tokenize(
            list(test_texts), max_length=int(self.args["max_length"])
        )

        # Convert labels to tensors
        train_labels = torch.tensor(train_labels, dtype=torch.long)
        test_labels = torch.tensor(test_labels, dtype=torch.long)

        batch_size = int(self.args["batch_size"])
        num_worker = int(self.args["num_worker"])
        train_dataset = DataLoader(
            TensorDataset(
                train_encodings["input_ids"],
                train_encodings["attention_mask"],
                train_labels,
            ),
            batch_size=batch_size,
            shuffle=True,
            num_workers=num_worker,
            persistent_workers=False,
        )
        val_dataset = DataLoader(
            TensorDataset(
                val_encodings["input_ids"], val_encodings["attention_mask"], test_labels
            ),
            batch_size=batch_size,
            num_workers=num_worker,
            persistent_workers=False,
        )
        return {
            "train_dataset": train_dataset,
            "val_dataset": val_dataset,
            "labels": data["labels"].values,
        }

    def load(self, results: dict[str, any]):
        torch.save(results["model"], self.get_save_filepath(f"{self.name}.pth"))

        # Create figure and axes for plotting
        fig, axs = plt.subplots(2, 1, figsize=(12, 16))
        fig.suptitle(self.name, fontsize=20, fontweight="bold")
        # Plot Training and Validation Loss

        axs[0].plot(results["train_accuracies"], "b--o", label="Training Accuracy")
        axs[0].plot(results["val_accuracies"], "r--o", label="Validation Accuracy")
        axs[0].set_title("Training & Validation Accuracy")
        axs[0].set_xlabel("Epoch")
        axs[0].set_ylabel("Accuracy")
        axs[0].legend()
        axs[0].axhline(y=1, color="orange", linestyle=":", linewidth=2)

        axs[1].plot(results["train_losses"], "b-o", label="Training Loss")
        axs[1].plot(results["val_losses"], "r-o", label="Validation Loss")
        axs[1].set_title("Training & Validation Loss")
        axs[1].set_xlabel("Epoch")
        axs[1].set_ylabel("Loss")
        axs[1].legend()
        axs[1].axhline(y=0, color="orange", linestyle=":", linewidth=2)

        # Adjust layout and save the plot
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        fig.savefig(self.get_save_filepath(f"{self.name}.png"))

        # Close the plot to avoid displaying it
        plt.close(fig)
