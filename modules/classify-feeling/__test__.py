import torch
import torch
from etl import ETL


def main():
    with ETL() as etl:
        etl.extract()
        labels = etl.labels.values

        model = torch.load(etl.args.model)
        model.eval()

        nums = 0
        if etl.args.nums is not None:
            nums = int(etl.args.nums)

        n = nums
        while True if nums == 0 else (n > 0):
            prompt_text = input("Enter Text:\n")
            if prompt_text == "":
                break

            encoded_input = etl.tokenize(prompt_text)
            with torch.no_grad():
                outputs = model(**encoded_input)
                logits = outputs.logits
                preds = torch.argmax(logits, dim=-1)
                print(labels[preds.cpu().numpy()[0]])
                print("-------------------------")
            if nums != 0:
                n -= 1


if __name__ == "__main__":
    main()
