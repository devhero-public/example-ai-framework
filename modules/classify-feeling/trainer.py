import torch
import time
from tqdm import tqdm
from toyai import Trainer as BaseTrainer
from transformers import BertForSequenceClassification


class Trainer(BaseTrainer):

    def train(self, epochs: int, data: dict[str, any]) -> dict[str, any]:
        train_dataset = data["train_dataset"]
        val_dataset = data["val_dataset"]

        model = BertForSequenceClassification.from_pretrained(
            "bert-base-multilingual-cased", num_labels=len(data["labels"])
        )

        total_params = 0
        for name, parameter in model.named_parameters():
            if not parameter.requires_grad:
                continue
            param = parameter.numel()
            total_params += param
            self.logger.info(f"{name}: {param} parameters")
        self.logger.info(f"Total trainable parameters: {total_params}")

        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        optimizer = torch.optim.AdamW(
            model.parameters(), lr=float(self.args["learning_rate"])
        )
        model.to(device)
        # Lists to store training and validation metrics
        train_losses, train_accuracies = [], []
        val_losses, val_accuracies = [], []

        # Record the start time of the training
        start_time = time.time()

        # Training loop for each epoch
        for epoch in range(epochs):
            self.logger.info("-----------------------------------------------------")
            self.logger.info(f"Epoch: {epoch + 1}/{epochs}")

            # Set model to training mode
            model.train()
            total_loss, total_accuracy = 0, 0
            batches = tqdm(train_dataset)

            # Iterate over each batch in the training data
            for i, batch in enumerate(batches):
                batch = tuple(b.to(device) for b in batch)
                inputs, masks, labels = batch

                # Zero the parameter gradients
                optimizer.zero_grad()

                # Forward pass
                outputs = model(inputs, attention_mask=masks, labels=labels)
                loss = outputs.loss
                logits = outputs.logits

                # Backward pass and optimization
                loss.backward()
                optimizer.step()

                # Accumulate loss and accuracy
                total_loss += loss.item()
                accuracy = self.compute_accuracy(logits, labels)
                total_accuracy += accuracy

            # Calculate average loss and accuracy for the epoch
            avg_train_loss = total_loss / len(train_dataset)
            avg_train_accuracy = total_accuracy / len(train_dataset)
            train_losses.append(avg_train_loss)
            train_accuracies.append(avg_train_accuracy)

            # Validate the model if validation data is available
            if val_dataset is not None:
                val_loss, val_accuracy = self.validate(
                    model=model,
                    val_dataset=val_dataset,
                    device=device,
                )
                val_losses.append(val_loss)
                val_accuracies.append(val_accuracy)

            # Record the end time and calculate elapsed time
            end_time = time.time()
            elapsed_time = end_time - start_time

            # Log training and validation metrics
            self.logger.info(
                f"Avg. Train: Loss = {avg_train_loss}, Accuracy = {avg_train_accuracy}"
            )
            self.logger.info(f"Validate: Loss = {val_loss}, Accuracy = {val_accuracy}")
            self.logger.info(f"Elapsed time = {elapsed_time}")

        return {
            "model": model,
            "train_losses": train_losses,
            "train_accuracies": train_accuracies,
            "val_losses": val_losses,
            "val_accuracies": val_accuracies,
            "epochs": epochs,
        }

    def validate(self, model, val_dataset, device):

        model.eval()  # Set model to evaluation mode
        val_loss, val_accuracy = 0, 0
        all_preds, all_labels = [], []

        # Iterate over each batch in the validation data
        for batch in val_dataset:
            batch = tuple(b.to(device) for b in batch)
            inputs, masks, labels = batch

            # Forward pass without gradient calculation
            with torch.no_grad():
                outputs = model(inputs, attention_mask=masks, labels=labels)

            loss = outputs.loss
            logits = outputs.logits
            val_loss += loss.item()
            preds = torch.argmax(logits, dim=1)

            # Accumulate predictions and labels for accuracy calculation
            all_preds.extend(preds.cpu().numpy())
            all_labels.extend(labels.cpu().numpy())
            val_accuracy += (preds == labels).cpu().numpy().mean()

        # Return average validation loss and accuracy
        return val_loss / len(val_dataset), val_accuracy / len(val_dataset)

    def compute_accuracy(self, logits, labels):
        # Calculate accuracy by comparing predicted and true labels
        preds = torch.argmax(logits, dim=-1)
        return torch.mean((preds == labels).type(torch.float)).item()
