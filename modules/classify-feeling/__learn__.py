from etl import ETL
from trainer import Trainer
import os

num_worker = 0
max_length = 16
test_size = 0.3
batch_size = 16
learning_rate = 5e-5


def main():
    with ETL(
        is_logs=True,
        args={
            "test_size": test_size,
            "max_length": max_length,
            "num_worker": num_worker,
            "learning_rate": learning_rate,
            "batch_size": batch_size,
        },
    ) as etl:
        # output_dir = "out-a"
        # epochs = 3
        epochs = int(etl.args["epochs"])
        parallelism = "false"
        os.environ["TOKENIZERS_PARALLELISM"] = parallelism

        etl.logger.info(f"name={etl.name}")
        etl.logger.info(f"epoch={epochs}")
        etl.logger.info(f"batch_size={batch_size}")
        etl.logger.info(f"max_length={max_length}")
        etl.logger.info(f"val_size={test_size * 100}%")
        etl.logger.info(f"learning_rate={learning_rate}")
        etl.train(Trainer, epochs=epochs)


if __name__ == "__main__":
    main()
