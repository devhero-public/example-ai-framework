
# Toyai Framework

Toyai is a powerful and flexible framework designed to simplify the development and testing of machine learning models. This guide will help you set up and use Toyai effectively.

## Installation

To install Toyai, use the following pip command:

```bash
pip install toyai
```

## Folder Structure

Your module should follow this structure:

```
modules/[module_name]/
    ├── __learn__.py        # Script to be run when learning
    ├── __test__.py         # Script to be run when testing
    ├── etl.py              # ETL script extending the base ETL
    └── trainner.py         # Trainer script extending the base Trainer
```

### Example `etl.py`

```python
from toyai import ETL as BaseETL, TrainResult

class ETL(BaseETL):
    # Your ETL code here
    pass
```

### Example `trainner.py`

```python
from toyai import Trainer as BaseTrainer, TrainResult

class Trainer(BaseTrainer):
    # Your Trainer code here
    pass
```

## Running Modules Locally

To learn a module locally, use the following command:

```bash
toyai learn module/[module_name] --xxx=2
```

You can pass extra arguments like `xxx=2` when running the script. These arguments can be accessed via `etl.args.xxx`.

To test a module locally, use the following command:

```bash
toyai test module/[module_name] --model=[pretained model filepath]
```

To build your module, use the following command:

```bash
toyai build module/[module_name]
```

Your built file will be created in the `build/[module_name]` directory.

To run a built module, use the following command:

```bash
toyai [learn|test] build/[module_name] extra_args
```

## Contact

If you have any questions, feel free to contact: [supanut.pgs@gmail.com](mailto:supanut.pgs@gmail.com)
